var gulp = require('gulp');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var imagemin = require('gulp-imagemin');
var htmlmin = require('gulp-htmlmin');
var usemin = require('gulp-usemin');

gulp.task('default', function () {
    gutil.log('Executando tarefa padrão...');
});

gulp.task('clean', function () {
    // Limpa o conteudo do diretorio
    return gulp.src(['dist-gulp/*']).pipe(clean());
});

gulp.task('minificar', ['clean'], function () {
    return gulp.src(['app/**/*.js'])      // Percorre todos arquivos JS da aplicação
        .pipe(uglify())                   // Minifica arquivos
        .pipe(gulp.dest('dist-gulp'));    // Salva na pasta de saída
});

gulp.task('watch', function () {
    gulp.watch('app/**/*.js', function (event) {
        gulp.run('dist');                 // Chama outra tarefa
    });
});

gulp.task('concatenar', ['minificar'], function () {
    gulp.src(['dist-gulp/**/*.js'])             // Percorre todos arquivos JS da aplicação
        .pipe(concat('all.js'))                 // Mescla arquivos em um unico
        .pipe(gulp.dest('dist-gulp/min'));      // salva na pasta de saída
});

var production = false;

gulp.task('compilar-css', function () {
    gulp.src('app/**/*.scss')                                     // Percorre todos arquivos SASS da aplicação
        .pipe(sass())                                             // Compila o SASS para CSS
        .pipe((production === true) ? cssmin() : gutil.noop())    // Minifica o CSS se for modo produção
        .pipe(gulp.dest('dist-gulp'));                            // Salva na pasta de saída
});

gulp.task('tratar-img', function () {
    gulp.src('app/assets/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist-gulp/assets/images/'))
});

// gulp.task('tratar-html', function() {
//     return gulp.src('app/index.html')
//         .pipe(htmlmin({collapseWhitespace: true}))
//         .pipe(gulp.dest('dist-gulp'))
// });

gulp.task('usemin', function() {
    return gulp.src('app/index.html')
        .pipe(usemin({
            //css: [ rev() ],
            //html: [ minifyHtml({ empty: true }) ],
            js: uglify(),
            //inlinejs: [ uglify() ],
            //inlinecss: [ minifyCss(), 'concat' ]
        }))
        .pipe(gulp.dest('dist-gulp'));
});

gulp.task('dist', ['minificar', 'concatenar', 'compilar-css', 'tratar-img']);