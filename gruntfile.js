module.exports = function(grunt) {

    grunt.initConfig({

        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'app',
                    src: '{,*/}*.scss',
                    dest: 'dist-grunt',
                    ext: '.css'
                }]
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['app/**/*.js'],
                dest: 'dist-grunt/application.js'
            }
        },

        uglify: {
            dist: {
                files: {
                    'dist/application.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },

        watch: {
            styles: {
                files: ['app/styles/{,*/}*.scss'],
                tasks: ['sass:dist', 'autoprefixer:dist']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['build']);

    grunt.registerTask('build', ['build']);

};